
#include <stdbool.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>



#include <unistd.h>  // sysconf() - get CPU count
//#include <stdint.h>


#define MAX_FILE_NAME 256
#define MAX_KEY_SIZE 290
#define MAX_VALUE_SIZE 192


int db_open();

int db_close();

int db_get_fs(const char *fsname, uint64_t *vid);

int db_put_fs(const char *fsname, uint64_t *vid);

int db_open_ino(uint64_t fs_id, uint64_t inode_id, const char *name, uint64_t *vid);

int db_get_ino(uint64_t fs_id, uint64_t inode_id, const char *name, uint64_t *vid);

int db_put_ino(uint64_t fs_id, uint64_t inode_id, const char *name, uint64_t *vid);

int db_delete_ino(uint64_t fs_id, uint64_t inode_id, const char *name);

void* db_iter_create(uint64_t fs_id, uint64_t inode_id, off_t offset);

int db_iter_destroy(void *iter_ptr);

int db_iter_seek(void *iter_ptr, off_t offset_cur, off_t offset_to);

int db_iter_next(void *iter_ptr, char *name, uint64_t *vid);

int db_iter_prev(void *iter_ptr, char *name, uint64_t *vid);

