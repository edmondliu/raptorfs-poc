/** @file
 *
 * Compile with:
 *
 *     g++ raptorfs.c db_ops.c -I ../../include/ -L ../../lib -o raptorfs -lpthread -lfuse3 -lrocksdb
 *
 *     g++ raptorfs.c db_ops.c -I ../include/ ../lib/libfuse3.so ../lib/librocksdb.a -o raptorfs -lpthread -lrt -lgflags -lz 
 *
 *     g++ -g raptorfs.c db_ops.c -I ../libfuse-master/include/ ../rocksdb/librocksdb.a ../libfuse-master/build/lib/libfuse3.so -lpthread -lrt -lgflags -lz -o raptorfs
 * 
 *     gcc -Wall raptorfs.c `pkg-config fuse3 --cflags --libs` -o raptorfs
 *
 */


#define _GNU_SOURCE
#define FUSE_USE_VERSION 31

#include <fuse_lowlevel.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <dirent.h>
#include <assert.h>
#include <errno.h>
#include <err.h>
#include <inttypes.h>
#include <pthread.h>
#include <sys/file.h>
#include <sys/xattr.h>

#include "db_ops.h"


/* We are re-using pointers to our `struct fs_inode` and `struct
   fs_dirp` elements as inodes. This means that we must be able to
   store uintptr_t values in a fuse_ino_t variable. The following
   incantation checks this condition at compile time. */
#if defined(__GNUC__) && (__GNUC__ > 4 || __GNUC__ == 4 && __GNUC_MINOR__ >= 6) && !defined __cplusplus
_Static_assert(sizeof(fuse_ino_t) >= sizeof(uintptr_t),
	       "fuse_ino_t too small to hold uintptr_t values!");
#else
struct _uintptr_to_must_hold_fuse_ino_t_dummy_struct \
	{ unsigned _uintptr_to_must_hold_fuse_ino_t:
			((sizeof(fuse_ino_t) >= sizeof(uintptr_t)) ? 1 : -1); };
#endif

enum {
	CACHE_NEVER,
	CACHE_NORMAL,
	CACHE_ALWAYS,
};



struct fs_inode {
	struct fs_inode *next; /* protected by lo->mutex */
	struct fs_inode *prev; /* protected by lo->mutex */
	int fd;
	bool is_symlink;
	ino_t ino;
	dev_t dev;
    mode_t    st_mode;
        
	uint64_t refcount; /* protected by lo->mutex */

    uint64_t fs_id;
    uint64_t parent_id;
    uint64_t inode_id;
    char name[MAX_FILE_NAME];
};


struct fs_data {
	pthread_mutex_t mutex;
	int debug;
	int writeback;
	int flock;
	int xattr;
	const char *source;
	double timeout;
	int cache;
	int timeout_set;
	struct fs_inode root; /* protected by lo->mutex */
};

struct fs_statvfs {
   unsigned long  f_bsize;    /* Filesystem block size */
   unsigned long  f_frsize;   /* Fragment size */
   fsblkcnt_t     f_blocks;   /* Size of fs in f_frsize units */
   fsblkcnt_t     f_bfree;    /* Number of free blocks */
   fsblkcnt_t     f_bavail;   /* Number of free blocks for
                                 unprivileged users */
   fsfilcnt_t     f_files;    /* Number of inodes */
   fsfilcnt_t     f_ffree;    /* Number of free inodes */
   fsfilcnt_t     f_favail;   /* Number of free inodes for
                                 unprivileged users */
   unsigned long  f_fsid;     /* Filesystem ID */
   unsigned long  f_flag;     /* Mount flags */
   unsigned long  f_namemax;  /* Maximum filename length */
};

struct fs_stat {
    //dev_t     st_dev;         // ID of device containing file 
    //ino_t     st_ino;         // inode number 
    mode_t    st_mode;        // protection 
    //nlink_t   st_nlink;       // number of hard links 
    uid_t     st_uid;         // user ID of owner 
    gid_t     st_gid;         // group ID of owner 
    //dev_t     st_rdev;        // device ID (if special file) 
    off_t     st_size;        // total size, in bytes 
    blksize_t st_blksize;     // blocksize for filesystem I/O 
    blkcnt_t  st_blocks;      // number of 512B blocks allocated 

    //Since Linux 2.6, the kernel supports nanosecond
       //precision for the following timestamp fields.
       //For the details before Linux 2.6, see NOTES. 

    struct timespec st_atim;  // time of last access 
    struct timespec st_mtim;  // time of last modification 
    struct timespec st_ctim;  // time of last status change 

//#define st_atime st_atim.tv_sec      // Backward compatibility 
//#define st_mtime st_mtim.tv_sec
//#define st_ctime st_ctim.tv_sec
};

enum fs_errors {
    ERR_COMMON      = -1,
    ERR_NO_MEMORY   = -2,

};


static const struct fuse_opt fs_opts[] = {
	{ "writeback",
	  offsetof(struct fs_data, writeback), 1 },
	{ "no_writeback",
	  offsetof(struct fs_data, writeback), 0 },
	{ "source=%s",
	  offsetof(struct fs_data, source), 0 },
	{ "flock",
	  offsetof(struct fs_data, flock), 1 },
	{ "no_flock",
	  offsetof(struct fs_data, flock), 0 },
	{ "xattr",
	  offsetof(struct fs_data, xattr), 1 },
	{ "no_xattr",
	  offsetof(struct fs_data, xattr), 0 },
	{ "timeout=%lf",
	  offsetof(struct fs_data, timeout), 0 },
	{ "timeout=",
	  offsetof(struct fs_data, timeout_set), 1 },
	{ "cache=never",
	  offsetof(struct fs_data, cache), CACHE_NEVER },
	{ "cache=auto",
	  offsetof(struct fs_data, cache), CACHE_NORMAL },
	{ "cache=always",
	  offsetof(struct fs_data, cache), CACHE_ALWAYS },

	FUSE_OPT_END
};

const char BlkPath[] = "/mnt/stor_blk";
int g_blk_fd;


int blk_init()
{
    int res = mkdir(BlkPath, 0755);
    if( res == -1)
        printf("blk dir exists.\n");

    g_blk_fd = open(BlkPath, O_NOFOLLOW | O_DIRECTORY);
    assert(g_blk_fd != -1);

    return 0;
}


int blk_ino_initstat(uint64_t vid, mode_t mode)
{
    int fd;
    char filename[17] = {0};
    struct stat buf;
    int ret;


	//generate stat and write:
    buf.st_dev = 0;         // ID of device containing file 
    buf.st_ino = vid;         // inode number 
    buf.st_mode = mode;       //100644;        // protection 

    if(mode & S_IFDIR){
        buf.st_nlink = 2;       // number of hard links
    }
    else {
        buf.st_nlink = 1;
    }
    
    buf.st_uid = getuid();         // user ID of owner 
    buf.st_gid = getgid();         // group ID of owner 
    buf.st_rdev = 0;        // device ID (if special file) 
    buf.st_size = 0;        // total size, in bytes 
    buf.st_blksize = 512;     // blocksize for filesystem I/O 
    buf.st_blocks = 0;      // number of 512B blocks allocated 

    struct timespec ts;
    ret = clock_gettime(CLOCK_REALTIME, &ts);
    assert(!ret);

    memcpy(&buf.st_atim, &ts, sizeof(struct timespec));
    memcpy(&buf.st_mtim, &ts, sizeof(struct timespec));
    memcpy(&buf.st_ctim, &ts, sizeof(struct timespec));  // time of last status change 

    sprintf(filename, "%llx-%8x", vid , 0xFFFFFFFF);
    printf("write stat for file(%s)\n", filename);

    fd = openat(g_blk_fd, filename, O_CREAT|O_RDWR & ~O_NOFOLLOW, 0755);
    if (fd == -1)
        return -1;

    ssize_t bytes;
    bytes = write(fd, &buf, sizeof(struct stat));
    if(bytes == -1)
        return -1;

    close(fd);

    return 0;
}

int blk_ino_setstat(uint64_t vid, struct stat *buf)
{
    int fd;
    char filename[17] = {0};

    sprintf(filename, "%llx-%8x", vid, 0xFFFFFFFF);
    printf("set stat for file(%s)\n", filename);

    fd = openat(g_blk_fd, filename, O_CREAT|O_RDWR & ~O_NOFOLLOW, 0755);
    if (fd == -1)
        return -1;

    ssize_t bytes;
    bytes = write(fd, buf, sizeof(struct stat));
    if(bytes == -1)
        return -1;

    close(fd);

    return 0;
}


int blk_ino_getstat(uint64_t vid, struct stat *buf)
{
    int fd;
    char filename[17] = {0};


    sprintf(filename, "%llx-%8x", vid, 0xFFFFFFFF);
    printf("get stat for file(%s)\n", filename);

    fd = openat(g_blk_fd, filename, O_CREAT|O_RDWR & ~O_NOFOLLOW, 0755);
    if (fd == -1)
        return -1;

    ssize_t bytes;
    bytes = read(fd, buf, sizeof(struct stat));
    if(bytes == -1)
        return -1;

    close(fd);
    
    return 0;
}

int blk_ino_write(uint64_t vid, int blk_num, off_t off, const char *buf, size_t size)
{
    int fd;
    char filename[17] = {0};
    int res;

    sprintf(filename, "%llx-%8x", vid, blk_num);
    printf("write data for file(%s)\n", filename);

    fd = openat(g_blk_fd, filename, O_CREAT|O_RDWR & ~O_NOFOLLOW, 0755);
    if (fd == -1)
        return -1;

    res = lseek(fd, off, SEEK_SET);
    assert(res != -1);
    
    ssize_t bytes;
    bytes = write(fd, buf, size);
    if(bytes == -1)
        return -1;

    close(fd);

    return bytes;

}

int blk_ino_read(uint64_t vid, int blk_num, off_t off, char *buf, size_t size)
{
    int fd;
    char filename[17] = {0};

    sprintf(filename, "%llx-%8x", vid, blk_num);
    printf("read data for file(%s)\n", filename);

    fd = openat(g_blk_fd, filename, O_CREAT|O_RDWR & ~O_NOFOLLOW, 0755);
    if (fd == -1)
        return -1;

    int res = lseek(fd, off, SEEK_SET);
    assert(res != -1);

    ssize_t bytes;
    bytes = read(fd, buf, size);
    if(bytes == -1)
        return -1;

    close(fd);

    return bytes;

}


int mds_getattr(struct fs_inode *ino, struct stat *buf)
{
    int ret;
    
    ret = blk_ino_getstat(ino->inode_id, buf);
    if(ret == -1)
        return -1;
    
    return 0;
}


int mds_setattr(struct fs_inode *ino, struct stat *attr, int valid)
{
    int ret;
    struct stat buf;
    
    ret = mds_getattr(ino, &buf);
    assert(!ret);    

    if (valid & FUSE_SET_ATTR_MODE) {
        buf.st_mode = attr->st_mode;
    }
    if (valid & (FUSE_SET_ATTR_UID | FUSE_SET_ATTR_GID)) {

        buf.st_uid = attr->st_uid;
        buf.st_gid = attr->st_gid;
    }
    if (valid & FUSE_SET_ATTR_SIZE) {
        buf.st_size = attr->st_size;
    }
    if (valid & FUSE_SET_ATTR_ATIME){
        buf.st_atim = attr->st_atim;
    }

    if (valid & FUSE_SET_ATTR_MTIME) {
        buf.st_mtim = attr->st_mtim;
    }

    ret = blk_ino_setstat(ino->inode_id, &buf); 
    assert(!ret);

    return 0;
}




int mds_statvfs(struct fs_inode *ino, struct statvfs *stbuf)
{
    //fake as constant first
    stbuf->f_bsize = 32*1024;    /* Filesystem block size */
    stbuf->f_frsize = 32*1024;   /* Fragment size */
    stbuf->f_blocks = 1024*1024;   /* Size of fs in f_frsize units */
    stbuf->f_bfree = 512*1024;    /* Number of free blocks */
    stbuf->f_bavail = 512*1024;   /* Number of free blocks for
                                  unprivileged users */
    stbuf->f_files = 999;    /* Number of inodes */
    stbuf->f_ffree = 10000000;    /* Number of free inodes */
    stbuf->f_favail = 10000000;   /* Number of free inodes for
                                  unprivileged users */
    stbuf->f_fsid = ino->fs_id;     /* Filesystem ID */
    stbuf->f_flag = 1024;     /* Mount flags */
    stbuf->f_namemax = 255;  /* Maximum filename length */

    return 0;
}


int mds_mountfs(struct fs_inode *ino_root, const char* fsname)
{
    //if fsname in db, mount fs; else create fs and mount 
    //key: fsname; value: get volumeid from master, use as fsid
    assert(fsname);
    int ret;
    uint64_t vid; 

    ret = blk_init();
    assert(!ret);
    
    ret = db_open();
    if(ret == -1){
        printf("db open Error\n ");
        return -1;
    }

    ret = db_get_fs(fsname, &vid);

    if( ret != 0 )
	{
        ret = db_put_fs(fsname, &vid);
        if(ret != 0)
            return -1;
	}

    ino_root->fs_id = vid;
    ino_root->inode_id = vid;
    ino_root->parent_id = 0;
    strcpy(ino_root->name, fsname);

    {
        struct stat buf;
        int newfd;
    
        newfd = open("/mnt/nas0", O_PATH | O_NOFOLLOW);
        assert(newfd != -1);
   
        ret = fstatat(newfd, "", &buf, AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW);
        assert(ret != -1);
        
        ret = blk_ino_initstat(vid, buf.st_mode);
        assert(!ret);

    }
        
    return 0;
}

void mds_umountfs(struct fs_data *fs)
{
    assert(fs);
    db_close();

    return ;
}

//int mds_mountfs(char* fsname)

int mds_open(struct fs_inode *ino, uint64_t *vid)
{
    int ret;

   // ret = db_get_ino(ino_parent, name, vid);
    //if( ret == 0 ){
      //  printf("mds_mkdir: can not create dir '%s': file exists\n", name);    
       // return -1;
    //}
    
    ret = db_open_ino(ino->fs_id, ino->parent_id, ino->name, vid);
    //assert(!ret);

    //ret = blk_ino_initstat(vid, mode);
    //assert(!ret);
    
    return ret;
}


int mds_mknod(struct fs_inode *ino_parent, const char *name, mode_t mode, uint64_t *vid)
{
    int ret;
    
    ret = db_put_ino(ino_parent->fs_id, ino_parent->inode_id, name, vid);
    assert(!ret);

    ret = blk_ino_initstat(*vid, mode );
    assert(!ret);
    
    return ret;
}


int mds_create(struct fs_inode *ino_parent, const char *name, mode_t mode, uint64_t *vid)
{
    int ret;
    
    ret = db_put_ino(ino_parent->fs_id, ino_parent->inode_id, name, vid);
    assert(!ret);

    ret = blk_ino_initstat(*vid, (mode | S_IFREG));
    assert(!ret);
    
    return ret;
}


int mds_mkdir(struct fs_inode *ino_parent, const char *name, mode_t mode, uint64_t *vid)
{
    int ret;

   // ret = db_get_ino(ino_parent, name, vid);
    //if( ret == 0 ){
      //  printf("mds_mkdir: can not create dir '%s': file exists\n", name);    
       // return -1;
    //}
    
    ret = db_put_ino(ino_parent->fs_id, ino_parent->inode_id, name, vid);
    assert(!ret);

    ret = blk_ino_initstat(*vid, (mode | S_IFDIR));
    assert(!ret);
    
    return ret;
}

int mds_rmdir(struct fs_inode *ino_parent, const char *name, uint64_t *vid)
{
    int ret;
    char cmd[PATH_MAX] = {0};
    int status = 0;
    
    ret = db_get_ino(ino_parent->fs_id, ino_parent->inode_id, name, vid);
    assert(!ret); 

    //TODO: To deallocate corresponding volume.(Last block for dir).
    sprintf(cmd, "rm %s/%llx-* -f", BlkPath, vid);
    status = system(cmd);
    if(status == -1) {
	//system error
	return (1);
    }else {
        if(WIFEXITED(status)) {
	    if(WEXITSTATUS(status) == 0){
	    //run successfully
            } else {
		//run failed
		return (WEXITSTATUS(status));
	    }
	} else {
	    //exit
	    return (WEXITSTATUS(status));
	}
    }

    ret = db_delete_ino(ino_parent->fs_id, ino_parent->inode_id, name);
    assert(!ret);

    return ret;
}


int mds_unlink(struct fs_inode *ino_parent, const char *name, uint64_t *vid)
{
    int ret;
    char cmd[PATH_MAX] = {0};
    int status = 0;
    
    ret = db_get_ino(ino_parent->fs_id, ino_parent->inode_id, name, vid);
    assert(!ret); 

    //TODO: To deallocate corresponding volume.(Last block for dir).
    sprintf(cmd, "rm %s/%llx-* -f", BlkPath, vid);
    status = system(cmd);
    if(status == -1) {
        //system error
        return (1);
    }else {
        if(WIFEXITED(status)) {
            if(WEXITSTATUS(status) == 0){
            //run successfully
            } else {
                //run failed
                return (WEXITSTATUS(status));
            }
        } else {
            //exit
            return (WEXITSTATUS(status));
        }
    }

    ret = db_delete_ino(ino_parent->fs_id, ino_parent->inode_id, name);
    assert(!ret);

    return ret;
}

/*
int mds_readdir()
{
}

int mds_create()
{

}
*/


static struct fs_data *fs_data(fuse_req_t req)
{
	return (struct fs_data *) fuse_req_userdata(req);
}

static struct fs_inode *fs_inode(fuse_req_t req, fuse_ino_t ino)
{
	if (ino == FUSE_ROOT_ID)
		return &fs_data(req)->root;
	else
		return (struct fs_inode *) (uintptr_t) ino;
}

static int fs_fd(fuse_req_t req, fuse_ino_t ino)
{
	return fs_inode(req, ino)->fd;
}

static bool fs_debug(fuse_req_t req)
{
	return fs_data(req)->debug != 0;
}

static void fs_init(void *userdata,
		    struct fuse_conn_info *conn)
{
	struct fs_data *lo = (struct fs_data*) userdata;

	if(conn->capable & FUSE_CAP_EXPORT_SUPPORT)
		conn->want |= FUSE_CAP_EXPORT_SUPPORT;

	if (lo->writeback &&
	    conn->capable & FUSE_CAP_WRITEBACK_CACHE) {
		if (lo->debug)
			fprintf(stderr, "fs_init: activating writeback\n");
		conn->want |= FUSE_CAP_WRITEBACK_CACHE;
	}
	if (lo->flock && conn->capable & FUSE_CAP_FLOCK_LOCKS) {
		if (lo->debug)
			fprintf(stderr, "fs_init: activating flock locks\n");
		conn->want |= FUSE_CAP_FLOCK_LOCKS;
	}
}

static void fs_getattr(fuse_req_t req, fuse_ino_t ino,
			     struct fuse_file_info *fi)
{
	int res;
	struct stat buf;
	struct fs_data *lo = fs_data(req);

	(void) fi;

	int newfd;

/*
	newfd = open("/mnt/nas2", O_PATH | O_NOFOLLOW);
	if (newfd == -1)
		return (void) fuse_reply_err(req, errno);

	res = fstatat(newfd, "", &buf, AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW);
*/
	//if (res == -1)
	//	goto out_err;

	//res = fstatat(fs_fd(req, ino), "", &buf, AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW);

    res = mds_getattr(fs_inode(req, ino), &buf);
	if (res == -1)
		return (void) fuse_reply_err(req, errno);

	fuse_reply_attr(req, &buf, lo->timeout);
}

static int utimensat_empty_nofollow(struct fs_inode *inode,
				    const struct timespec *tv)
{
	int res;
	char procname[64];

	if (inode->is_symlink) {
		res = utimensat(inode->fd, "", tv,
				AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW);
		if (res == -1 && errno == EINVAL) {
			/* Sorry, no race free way to set times on symlink. */
			errno = EPERM;
		}
		return res;
	}
	sprintf(procname, "/proc/self/fd/%i", inode->fd);

	return utimensat(AT_FDCWD, procname, tv, 0);
}

static void fs_setattr(fuse_req_t req, fuse_ino_t ino, struct stat *attr,
		       int valid, struct fuse_file_info *fi)
{
	int saverr;
	char procname[64];
	struct fs_inode *inode = fs_inode(req, ino);
	int ifd = inode->fd;
	int res;

    res = mds_setattr(inode, attr, valid);   
    assert(!res);
    
/*
	if (valid & FUSE_SET_ATTR_MODE) {
		if (fi) {
			res = fchmod(fi->fh, attr->st_mode);
		} else {
			sprintf(procname, "/proc/self/fd/%i", ifd);
			res = chmod(procname, attr->st_mode);
		}
		if (res == -1)
			goto out_err;
	}
	if (valid & (FUSE_SET_ATTR_UID | FUSE_SET_ATTR_GID)) {
		uid_t uid = (valid & FUSE_SET_ATTR_UID) ?
			attr->st_uid : (uid_t) -1;
		gid_t gid = (valid & FUSE_SET_ATTR_GID) ?
			attr->st_gid : (gid_t) -1;

		res = fchownat(ifd, "", uid, gid,
			       AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW);
		if (res == -1)
			goto out_err;
	}
	if (valid & FUSE_SET_ATTR_SIZE) {
		if (fi) {
			res = ftruncate(fi->fh, attr->st_size);
		} else {
			sprintf(procname, "/proc/self/fd/%i", ifd);
			res = truncate(procname, attr->st_size);
		}
		if (res == -1)
			goto out_err;
	}
	if (valid & (FUSE_SET_ATTR_ATIME | FUSE_SET_ATTR_MTIME)) {
		struct timespec tv[2];

		tv[0].tv_sec = 0;
		tv[1].tv_sec = 0;
		tv[0].tv_nsec = UTIME_OMIT;
		tv[1].tv_nsec = UTIME_OMIT;

		if (valid & FUSE_SET_ATTR_ATIME_NOW)
			tv[0].tv_nsec = UTIME_NOW;
		else if (valid & FUSE_SET_ATTR_ATIME)
			tv[0] = attr->st_atim;

		if (valid & FUSE_SET_ATTR_MTIME_NOW)
			tv[1].tv_nsec = UTIME_NOW;
		else if (valid & FUSE_SET_ATTR_MTIME)
			tv[1] = attr->st_mtim;

		if (fi)
			res = futimens(fi->fh, tv);
		else
			res = utimensat_empty_nofollow(inode, tv);
		if (res == -1)
			goto out_err;
	}
*/
	return fs_getattr(req, ino, fi);

out_err:
	saverr = errno;
	fuse_reply_err(req, saverr);
}

/*
static struct fs_inode *fs_find(struct fs_data *lo, struct stat *st)
{
	struct fs_inode *p;
	struct fs_inode *ret = NULL;

	pthread_mutex_lock(&lo->mutex);
	for (p = lo->root.next; p != &lo->root; p = p->next) {
		if (p->ino == st->st_ino && p->dev == st->st_dev) {
			assert(p->refcount > 0);
			ret = p;
			ret->refcount++;
			break;
		}
	}
	pthread_mutex_unlock(&lo->mutex);
	return ret;
}
*/

static struct fs_inode *fs_find(struct fs_data *lo, struct fs_inode *ino_parent, const char *name)
{
	struct fs_inode *p;
	struct fs_inode *ret = NULL;

	pthread_mutex_lock(&lo->mutex);
	for (p = lo->root.next; p != &lo->root; p = p->next) {
		if( (p->parent_id == ino_parent->inode_id) && ( strcmp(p->name, name) == 0) ){
			assert(p->refcount > 0);
			ret = p;
			ret->refcount++;
			break;
		}
	}
	pthread_mutex_unlock(&lo->mutex);
	return ret;
}


static int fs_do_lookup(fuse_req_t req, fuse_ino_t parent, const char *name,
			 struct fuse_entry_param *e)
{
	//int newfd;
	int res;
	int saverr;
	struct fs_data *lo = fs_data(req);
	struct fs_inode *inode;
    struct fs_inode *ino_parent = fs_inode(req, parent);

	memset(e, 0, sizeof(*e));
	e->attr_timeout = lo->timeout;
	e->entry_timeout = lo->timeout;

/*
	newfd = openat(fs_fd(req, parent), name, O_PATH | O_NOFOLLOW);
	if (newfd == -1)
		goto out_err;

	res = fstatat(newfd, "", &e->attr, AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW);
	if (res == -1)
		goto out_err;
*/
/*

    //(void) fi;
    int newfd;

    newfd = open("/mnt/nas2", O_PATH | O_NOFOLLOW);
    if (newfd == -1)
        goto out_err;

    res = fstatat(newfd, "", &e->attr, AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW);

		
//*/

/*
    size_t len;
    char *err = NULL;
    char key_str[MAX_KEY_SIZE];   
    uint64_t vid; 
    rocksdb_readoptions_t *readoptions = rocksdb_readoptions_create();

    sprintf(key_str,"%llx/%llx/%s", ino_parent->fs_id, ino_parent->inode_id, name);
    char *vid_str = rocksdb_get(db, readoptions, key_str, strlen(key_str), &len, &err);
    assert(!err);
    if(vid_str == NULL)
        goto out_err;
    
    vid = strtoull(vid_str, NULL, 16);
    free(vid_str);
*/

    uint64_t vid;
    res = db_get_ino(ino_parent->fs_id, ino_parent->inode_id, name, &vid);
    if(res !=0 )
        goto out_err;

    res = blk_ino_getstat(vid, &e->attr);
    assert(!res);

	//inode = fs_find(fs_data(req), &e->attr);
    inode = fs_find(fs_data(req), ino_parent, name);

    if (inode) {
		//close(newfd);
		//newfd = -1;
	} else {
		struct fs_inode *prev, *next;

		saverr = ENOMEM;
		inode = (struct fs_inode*)calloc(1, sizeof(struct fs_inode));
		if (!inode)
			goto out_err;

		//inode->is_symlink = 0; //S_ISLNK(e->attr.st_mode);
		inode->refcount = 1;
		//inode->fd = newfd;
		//inode->ino = e->attr.st_ino;
		//inode->dev = e->attr.st_dev;

		pthread_mutex_lock(&lo->mutex);
		prev = &lo->root;
		next = prev->next;
		next->prev = inode;
		inode->next = next;
		inode->prev = prev;
		prev->next = inode;
		pthread_mutex_unlock(&lo->mutex);

        inode->fs_id = ino_parent->fs_id;
        inode->parent_id = ino_parent->inode_id;
        inode->inode_id = vid;
        sprintf(inode->name, name, sizeof(name));
        
	}
	e->ino = (uintptr_t) inode;

	if (fs_debug(req))
		fprintf(stderr, "  %lli/%s -> %lli\n",
			(unsigned long long) parent, name, (unsigned long long) e->ino);

	return 0;

out_err:
	saverr = errno;
//	if (newfd != -1)
	//	close(newfd);
	return saverr;
}

static void fs_lookup(fuse_req_t req, fuse_ino_t parent, const char *name)
{
	struct fuse_entry_param e;
	int err;

	if (fs_debug(req))
		fprintf(stderr, "fs_lookup(parent=%" PRIu64 ", name=%s)\n",
			parent, name);

	err = fs_do_lookup(req, parent, name, &e);
	if (err)
		fuse_reply_err(req, err);
	else
		fuse_reply_entry(req, &e);
}

static void fs_mknod_symlink(fuse_req_t req, fuse_ino_t parent,
			     const char *name, mode_t mode, dev_t rdev,
			     const char *link)
{
	int newfd = -1;
	int res;
	int saverr;
	struct fs_inode *ino_parent = fs_inode(req, parent);
	//struct fs_inode *inode;
	//struct fs_inode *dir = fs_inode(req, parent);
	struct fuse_entry_param e;
    	uint64_t vid;

	saverr = ENOMEM;
	//inode = (struct fs_inode*)calloc(1, sizeof(struct fs_inode));
	//if (!inode)
	//	goto out;

	if (S_ISDIR(mode)) {
		//res = mkdirat(dir->fd, name, mode);
		res = mds_mkdir( ino_parent, name, mode, &vid);
	} else if (S_ISLNK(mode)) {
		//res = symlinkat(link, dir->fd, name);

        //TODO: to support symlink later.
        //res = mds_mknod( inode->fs_id, inode->inode_id, name, mode, &vid);
	}else {
		//res = mknodat(dir->fd, name, mode, rdev);
		res = mds_mknod( ino_parent, name, mode, &vid);
    }

    saverr = errno;
	if (res == -1)
		goto out;

	saverr = fs_do_lookup(req, parent, name, &e);
	if (saverr)
		goto out;

	if (fs_debug(req))
		fprintf(stderr, "  %lli/%s -> %lli\n",
			(unsigned long long) parent, name, (unsigned long long) e.ino);

	fuse_reply_entry(req, &e);
	return;

out:
	if (newfd != -1)
		close(newfd);
//	free(inode);
	fuse_reply_err(req, saverr);
}

static void fs_mknod(fuse_req_t req, fuse_ino_t parent,
		     const char *name, mode_t mode, dev_t rdev)
{
	fs_mknod_symlink(req, parent, name, mode, rdev, NULL);
}

static void fs_mkdir(fuse_req_t req, fuse_ino_t parent, const char *name,
		     mode_t mode)
{
	fs_mknod_symlink(req, parent, name, S_IFDIR | mode, 0, NULL);

/*
    //int newfd = -1;
	int res;
	int saverr;
	struct fs_inode *ino_parent = fs_inode(req, parent);
	struct fuse_entry_param e;
    uint64_t vid;

	//res = mkdirat(dir->fd, name, mode);

    res = mds_mkdir( ino_parent, name, mode, &vid);
	saverr = errno;
	if (res == -1)
		goto out;

	saverr = fs_do_lookup(req, parent, name, &e);
	if (saverr)
		goto out;

	if (fs_debug(req))
		fprintf(stderr, "  %lli/%s -> %lli\n",
			(unsigned long long) parent, name, (unsigned long long) e.ino);

	fuse_reply_entry(req, &e);
	return;

out:
	//if (newfd != -1)
		//close(newfd);

	fuse_reply_err(req, saverr);
*/
}

static void fs_symlink(fuse_req_t req, const char *link,
		       fuse_ino_t parent, const char *name)
{
	fs_mknod_symlink(req, parent, name, S_IFLNK, 0, link);
}

static int linkat_empty_nofollow(struct fs_inode *inode, int dfd,
				 const char *name)
{
	int res;
	char procname[64];

	if (inode->is_symlink) {
		res = linkat(inode->fd, "", dfd, name, AT_EMPTY_PATH);
		if (res == -1 && (errno == ENOENT || errno == EINVAL)) {
			/* Sorry, no race free way to hard-link a symlink. */
			errno = EPERM;
		}
		return res;
	}

	sprintf(procname, "/proc/self/fd/%i", inode->fd);

	return linkat(AT_FDCWD, procname, dfd, name, AT_SYMLINK_FOLLOW);
}

static void fs_link(fuse_req_t req, fuse_ino_t ino, fuse_ino_t parent,
		    const char *name)
{
	int res;
	struct fs_data *lo = fs_data(req);
	struct fs_inode *inode = fs_inode(req, ino);
	struct fuse_entry_param e;
	int saverr;

	memset(&e, 0, sizeof(struct fuse_entry_param));
	e.attr_timeout = lo->timeout;
	e.entry_timeout = lo->timeout;

	res = linkat_empty_nofollow(inode, fs_fd(req, parent), name);
	if (res == -1)
		goto out_err;

	res = fstatat(inode->fd, "", &e.attr, AT_EMPTY_PATH | AT_SYMLINK_NOFOLLOW);
	if (res == -1)
		goto out_err;

	pthread_mutex_lock(&lo->mutex);
	inode->refcount++;
	pthread_mutex_unlock(&lo->mutex);
	e.ino = (uintptr_t) inode;

	if (fs_debug(req))
		fprintf(stderr, "  %lli/%s -> %lli\n",
			(unsigned long long) parent, name,
			(unsigned long long) e.ino);

	fuse_reply_entry(req, &e);
	return;

out_err:
	saverr = errno;
	fuse_reply_err(req, saverr);
}

static void fs_rmdir(fuse_req_t req, fuse_ino_t parent, const char *name)
{
	int res;
	struct fs_inode *ino_parent = fs_inode(req, parent);
	uint64_t vid;

        res = mds_rmdir(ino_parent, name, &vid);
	//res = unlinkat(fs_fd(req, parent), name, AT_REMOVEDIR);

	fuse_reply_err(req, res == -1 ? errno : 0);
}

static void fs_rename(fuse_req_t req, fuse_ino_t parent, const char *name,
		      fuse_ino_t newparent, const char *newname,
		      unsigned int flags)
{
	int res;

	if (flags) {
		fuse_reply_err(req, EINVAL);
		return;
	}

	res = renameat(fs_fd(req, parent), name,
			fs_fd(req, newparent), newname);

	fuse_reply_err(req, res == -1 ? errno : 0);
}

static void fs_unlink(fuse_req_t req, fuse_ino_t parent, const char *name)
{
	int res;
        struct fs_inode *ino_parent = fs_inode(req, parent);
        uint64_t vid;

        res = mds_unlink(ino_parent, name, &vid);
//	res = unlinkat(fs_fd(req, parent), name, 0);

	fuse_reply_err(req, res == -1 ? errno : 0);
}

static void unref_inode(struct fs_data *lo, struct fs_inode *inode, uint64_t n)
{
	if (!inode)
		return;

	pthread_mutex_lock(&lo->mutex);
	assert(inode->refcount >= n);
	inode->refcount -= n;
	if (!inode->refcount) {
		struct fs_inode *prev, *next;

		prev = inode->prev;
		next = inode->next;
		next->prev = prev;
		prev->next = next;

		pthread_mutex_unlock(&lo->mutex);
		close(inode->fd);
		free(inode);

	} else {
		pthread_mutex_unlock(&lo->mutex);
	}
}

static void fs_forget_one(fuse_req_t req, fuse_ino_t ino, uint64_t nlookup)
{
	struct fs_data *lo = fs_data(req);
	struct fs_inode *inode = fs_inode(req, ino);

	if (fs_debug(req)) {
		fprintf(stderr, "  forget %lli %lli -%lli\n",
			(unsigned long long) ino,
			(unsigned long long) inode->refcount,
			(unsigned long long) nlookup);
	}

	unref_inode(lo, inode, nlookup);
}

static void fs_forget(fuse_req_t req, fuse_ino_t ino, uint64_t nlookup)
{
	fs_forget_one(req, ino, nlookup);
	fuse_reply_none(req);
}

static void fs_forget_multi(fuse_req_t req, size_t count,
				struct fuse_forget_data *forgets)
{
	int i;

	for (i = 0; i < count; i++)
		fs_forget_one(req, forgets[i].ino, forgets[i].nlookup);
	fuse_reply_none(req);
}

static void fs_readlink(fuse_req_t req, fuse_ino_t ino)
{
	char buf[PATH_MAX + 1];
	int res;

	res = readlinkat(fs_fd(req, ino), "", buf, sizeof(buf));
	if (res == -1)
		return (void) fuse_reply_err(req, errno);

	if (res == sizeof(buf))
		return (void) fuse_reply_err(req, ENAMETOOLONG);

	buf[res] = '\0';

	fuse_reply_readlink(req, buf);
}

struct fs_dirp {
	//int fd;
	//DIR *dp;

    void *iter;

    //TODO: change entry to d_ino and d_type directly;
    struct dirent *entry;
	off_t offset;
};

static struct fs_dirp *fs_dirp(struct fuse_file_info *fi)
{
	return (struct fs_dirp *) (uintptr_t) fi->fh;
}

static void fs_opendir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	int error = ENOMEM;
    struct fs_data *lo = fs_data(req);
	struct fs_dirp *d = (struct fs_dirp*)calloc(1, sizeof(struct fs_dirp));
	if (d == NULL)
		goto out_err;

    struct fs_inode *inode = fs_inode(req, ino);
    d->iter = db_iter_create( inode->fs_id, inode->inode_id, 0 );
	d->offset = 0;
	d->entry = NULL;

/*
	d->fd = openat(fs_fd(req, ino), ".", O_RDONLY);
	if (d->fd == -1)
		goto out_errno;

	d->dp = fdopendir(d->fd);
	if (d->dp == NULL)
		goto out_errno;
*/

	fi->fh = (uintptr_t) d;
	if (lo->cache == CACHE_ALWAYS)
		fi->keep_cache = 1;
	fuse_reply_open(req, fi);
	return;

out_errno:
	error = errno;
out_err:
	if (d) {
		//if (d->fd != -1)
			//close(d->fd);
		free(d);
	}
	fuse_reply_err(req, error);
}

static int is_dot_or_dotdot(const char *name)
{
	return name[0] == '.' && (name[1] == '\0' ||
				  (name[1] == '.' && name[2] == '\0'));
}

static void fs_do_readdir(fuse_req_t req, fuse_ino_t ino, size_t size,
			  off_t offset, struct fuse_file_info *fi, int plus)
{
	struct fs_dirp *d = fs_dirp(fi);
	char *buf;
	char *p;
	size_t rem;
	int err;
    int ret;
	off_t nextoff = offset;
    struct fs_inode *inode = fs_inode(req, ino);

	//(void) ino;

	buf = (char*)calloc(1, size);
	if (!buf)
		return (void) fuse_reply_err(req, ENOMEM);

	if (offset != d->offset) {
		//seekdir(d->dp, offset);
		printf("do_readdir mismatch: offset(%llu), d->offset(%llu)\n", offset, d->offset);
		db_iter_seek(d->iter, d->offset, offset);
		d->entry = NULL;
		d->offset = offset;
	}

	p = buf;
	rem = size;
    //rocksdb_prefix will get all the key after seek. let's compare by self first
    char prefix_str[MAX_KEY_SIZE];
    sprintf(prefix_str,"%llx/%llx/", inode->fs_id, inode->inode_id);    
   
	printf("do_readdir start: prefix(%s), offset(%llu), size(%llu)\n ", prefix_str, offset, size ); 
	while (1) {
		size_t entsize;
		//off_t nextoff;
		char key_str[MAX_KEY_SIZE];
        char *name;
        uint64_t vid;
        int is_end;

	    memset(key_str, 0, MAX_KEY_SIZE);
        is_end = db_iter_next(d->iter, key_str, &vid);
        printf("prefix(%s),key_str(%s)\n", prefix_str, key_str);

        if ( (is_end) 
            || strncmp(prefix_str, key_str, 34) != 0 )
        {
            errno = 0;
	        break;
	    }

        name = key_str + 34;
        nextoff++;
        
/*
		if (!d->entry) {
			errno = 0;
			d->entry = readdir(d->dp);
			if (!d->entry) {
				if (errno && rem == size) {
					err = errno;
					goto error;
				}
				break;
			}
		}
		nextoff = telldir(d->dp);
		name = d->entry->d_name;
*/

		if (plus) {
			struct fuse_entry_param e;

			if (is_dot_or_dotdot(name)) {
                e.attr.st_ino = vid;
                e.attr.st_mode = S_IFDIR;
				//e = (struct fuse_entry_param) {
				//	.attr.st_ino = vid,
                    // fake as 0, find a place to store st_mode/d_type
				//	.attr.st_mode = 0,
				//};
			} else {
				err = fs_do_lookup(req, ino, name, &e);
				if (err)
					goto error;
			}

			entsize = fuse_add_direntry_plus(req, p, rem, name,
							 &e, nextoff);
		} else {
		
			struct stat st, st_0;

            //TODO: to save d->entry->d_type in KV so this getstat() could be avoid.
            int res = blk_ino_getstat(vid, &st_0);
            assert(!res);

            st.st_ino = vid;
            st.st_mode = st_0.st_mode & 0xFFFFF000;

			entsize = fuse_add_direntry(req, p, rem, name,
						    &st, nextoff);
            
        /*
			struct stat st = {
				.st_ino = vid,
				.st_mode = d->entry->d_type << 12,
			};
			entsize = fuse_add_direntry(req, p, rem, name,
						    &st, nextoff);
		*/
		}
		if (entsize > rem){
			//re-position the iter back so the entry could be get again in next readdir loop
			db_iter_prev(d->iter, key_str, &vid);
			break;
		}
		p += entsize;
		rem -= entsize;

		d->entry = NULL;
		d->offset = nextoff;
	}

	printf("do_readdir end: prefix(%s), d->offset(%llu), size(%llu), rem(%u)\n ", prefix_str, d->offset, size, rem );
	fuse_reply_buf(req, buf, size - rem);
	free(buf);
	return;

error:
    if(buf)
	    free(buf);

    fuse_reply_err(req, err);
}

static void fs_readdir(fuse_req_t req, fuse_ino_t ino, size_t size,
		       off_t offset, struct fuse_file_info *fi)
{
	fs_do_readdir(req, ino, size, offset, fi, 0);
}

static void fs_readdirplus(fuse_req_t req, fuse_ino_t ino, size_t size,
			   off_t offset, struct fuse_file_info *fi)
{
	fs_do_readdir(req, ino, size, offset, fi, 1);
}

static void fs_releasedir(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	struct fs_dirp *d = fs_dirp(fi);
	(void) ino;
	//closedir(d->dp);

    db_iter_destroy(d->iter);

    free(d);
	fuse_reply_err(req, 0);
}

static void fs_create(fuse_req_t req, fuse_ino_t parent, const char *name,
		      mode_t mode, struct fuse_file_info *fi)
{
	int fd;
	struct fuse_entry_param e;
	int err;
    uint64_t vid;

	if (fs_debug(req))
		fprintf(stderr, "fs_create(parent=%" PRIu64 ", name=%s)\n",
			parent, name);

    int res = mds_create( fs_inode(req, parent), name, mode, &vid);
    if (res == -1)
        fuse_reply_err(req, errno);

/*
	fd = openat(fs_fd(req, parent), name,
		    (fi->flags | O_CREAT) & ~O_NOFOLLOW, mode);
	if (fd == -1)
		return (void) fuse_reply_err(req, errno);

	fi->fh = fd;
*/
	err = fs_do_lookup(req, parent, name, &e);
	if (err)
		fuse_reply_err(req, err);
	else
		fuse_reply_create(req, &e, fi);
}

static void fs_fsyncdir(fuse_req_t req, fuse_ino_t ino, int datasync,
			struct fuse_file_info *fi)
{
	int res = 0;
	/*
	int fd = dirfd(fs_dirp(fi)->dp);
	(void) ino;
	if (datasync)
		res = fdatasync(fd);
	else
		res = fsync(fd);
	*/
	fuse_reply_err(req, res == -1 ? errno : 0);
}

static void fs_open(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	int fd;
	char buf[64];
	struct fs_data *lo = fs_data(req);

	if (fs_debug(req))
		fprintf(stderr, "fs_open(ino=%" PRIu64 ", flags=%d)\n",
			ino, fi->flags);

	/* With writeback cache, kernel may send read requests even
	   when userspace opened write-only */
	if (lo->writeback && (fi->flags & O_ACCMODE) == O_WRONLY) {
		fi->flags &= ~O_ACCMODE;
		fi->flags |= O_RDWR;
	}

	/* With writeback cache, O_APPEND is handled by the kernel.
	   This breaks atomicity (since the file may change in the
	   underlying filesystem, so that the kernel's idea of the
	   end of the file isn't accurate anymore). In this example,
	   we just accept that. A more rigorous filesystem may want
	   to return an error here */
	if (lo->writeback && (fi->flags & O_APPEND))
		fi->flags &= ~O_APPEND;

	//sprintf(buf, "/proc/self/fd/%i", fs_fd(req, ino));

    //ignore flags first. Let's deal with it later.
    uint64_t vid;
    struct fs_inode *inode = fs_inode(req, ino);
    
    fd = mds_open( inode, &vid);
	if (fd == -1)
		return (void) fuse_reply_err(req, errno);    

    fi->fh = (int)vid;
    /*
	fd = open(buf, fi->flags & ~O_NOFOLLOW);
	if (fd == -1)
		return (void) fuse_reply_err(req, errno);

	fi->fh = fd;
	*/
	if (lo->cache == CACHE_NEVER)
		fi->direct_io = 1;
	else if (lo->cache == CACHE_ALWAYS)
		fi->keep_cache = 1;
	fuse_reply_open(req, fi);
}

static void fs_release(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	(void) ino;

	//close(fi->fh);
	fuse_reply_err(req, 0);
}

static void fs_flush(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi)
{
	int res;
	(void) ino;
	res = close(dup(fi->fh));
	fuse_reply_err(req, res == -1 ? errno : 0);
}

static void fs_fsync(fuse_req_t req, fuse_ino_t ino, int datasync,
		     struct fuse_file_info *fi)
{
	int res;
	(void) ino;
	if (datasync)
		res = fdatasync(fi->fh);
	else
		res = fsync(fi->fh);
	fuse_reply_err(req, res == -1 ? errno : 0);
}

/*
static void fs_read(fuse_req_t req, fuse_ino_t ino, size_t size,
		    off_t offset, struct fuse_file_info *fi)
{
	struct fuse_bufvec buf = FUSE_BUFVEC_INIT(size);

	if (fs_debug(req))
		fprintf(stderr, "fs_read(ino=%" PRIu64 ", size=%zd, "
			"off=%lu)\n", ino, size, (unsigned long) offset);

	buf.buf[0].flags = (fuse_buf_flags)(FUSE_BUF_IS_FD | FUSE_BUF_FD_SEEK);
	buf.buf[0].fd = fi->fh;
	buf.buf[0].pos = offset;

	fuse_reply_data(req, &buf, FUSE_BUF_SPLICE_MOVE);
}
*/

#define BLK_SIZE    (64*1024*1024)


static void fs_read(fuse_req_t req, fuse_ino_t ino, size_t size, off_t off,
			 struct fuse_file_info *fi)
{
    (void)fi;
    char  *buf = (char*)calloc(1, size);
	if (!buf)
		return (void) fuse_reply_err(req, ENOMEM);
    

    struct fs_inode *inode = fs_inode(req, ino);
    uint64_t vid = inode->inode_id;

    size_t size_left = size;
    off_t off_cur = off;
    char *buf_cur = buf;


    while(size_left > 0){
        int bytes = 0;
        int blk_num = off_cur/BLK_SIZE;
        int size_cur = ((off_cur%BLK_SIZE+ size_left) > BLK_SIZE) 
            ? (BLK_SIZE - (off_cur%BLK_SIZE)) : size_left;

        bytes = blk_ino_read( vid, blk_num, (off_cur%BLK_SIZE), buf_cur ,size_cur);
        assert(bytes != -1);

        if(bytes == 0 ) //reach end of file
            break;
        
        off_cur += bytes;
        size_left -= bytes;
        buf_cur += bytes;
    }

    fuse_reply_buf(req, buf, size - size_left);
    free(buf);
}



static void fs_write(fuse_req_t req, fuse_ino_t ino, const char *buf, size_t size,
			  off_t off, struct fuse_file_info *fi)
{
	(void)fi;

    struct fs_inode *inode = fs_inode(req, ino);
    uint64_t vid = inode->inode_id;

    size_t size_left = size;
    off_t off_cur = off;
    const char *buf_cur = buf;
    

    while(size_left > 0){
        int bytes = 0;
        int blk_num = off_cur/BLK_SIZE;
        int size_cur = ((off_cur%BLK_SIZE+ size_left) > BLK_SIZE) 
            ? (BLK_SIZE - (off_cur%BLK_SIZE)) : size_left;

        bytes = blk_ino_write( vid, blk_num, (off_cur%BLK_SIZE), buf_cur ,size_cur);
        assert(bytes != -1);

        off_cur += bytes;
        size_left -= bytes;
        buf_cur += bytes;
    }

    
     int valid = 0;
     int ret;
     struct stat attr;
     
     ret = blk_ino_getstat(vid, &attr);
     assert(!ret);    
    
     valid |= FUSE_SET_ATTR_SIZE;
             
         attr.st_size = ( (off + size) > attr.st_size ) ? (off + size) : attr.st_size;
    /*
     if (valid & FUSE_SET_ATTR_ATIME){
         buf.st_atim = attr->st_atim;
     }
    
     if (valid & FUSE_SET_ATTR_MTIME) {
         buf.st_mtim = attr->st_mtim;
     }
     */
    
     ret = blk_ino_setstat(vid, &attr); 
     assert(!ret);


    fuse_reply_write(req, size);
}

/*
static void fs_write_buf(fuse_req_t req, fuse_ino_t ino,
			 struct fuse_bufvec *in_buf, off_t off,
			 struct fuse_file_info *fi)
{
	(void) ino;
	ssize_t res;
	struct fuse_bufvec out_buf = FUSE_BUFVEC_INIT(fuse_buf_size(in_buf));

	out_buf.buf[0].flags = (fuse_buf_flags)(FUSE_BUF_IS_FD | FUSE_BUF_FD_SEEK);
	out_buf.buf[0].fd = fi->fh;
	out_buf.buf[0].pos = off;

	if (fs_debug(req))
		fprintf(stderr, "fs_write(ino=%" PRIu64 ", size=%zd, off=%lu)\n",
			ino, out_buf.buf[0].size, (unsigned long) off);

	res = fuse_buf_copy(&out_buf, in_buf, (fuse_buf_copy_flags)0);
	if(res < 0)
		fuse_reply_err(req, -res);
	else
		fuse_reply_write(req, (size_t) res);
    
}
*/

static void fs_statfs(fuse_req_t req, fuse_ino_t ino)
{
	int res;
	struct statvfs stbuf;

	//res = fstatvfs(fs_fd(req, ino), &stbuf);
	res = mds_statvfs( fs_inode(req, ino), &stbuf);
	if (res == -1)
		fuse_reply_err(req, errno);
	else
		fuse_reply_statfs(req, &stbuf);
}

static void fs_fallocate(fuse_req_t req, fuse_ino_t ino, int mode,
			 off_t offset, off_t length, struct fuse_file_info *fi)
{
	int err;
	(void) ino;

	if (mode) {
		fuse_reply_err(req, EOPNOTSUPP);
		return;
	}

	err = posix_fallocate(fi->fh, offset, length);

	fuse_reply_err(req, err);
}

static void fs_flock(fuse_req_t req, fuse_ino_t ino, struct fuse_file_info *fi,
		     int op)
{
	int res;
	(void) ino;

	res = flock(fi->fh, op);

	fuse_reply_err(req, res == -1 ? errno : 0);
}

static void fs_getxattr(fuse_req_t req, fuse_ino_t ino, const char *name,
			size_t size)
{
	char *value = NULL;
	char procname[64];
	struct fs_inode *inode = fs_inode(req, ino);
	ssize_t ret;
	int saverr;

	saverr = ENOSYS;
	if (!fs_data(req)->xattr)
		goto out;

	if (fs_debug(req)) {
		fprintf(stderr, "fs_getxattr(ino=%" PRIu64 ", name=%s size=%zd)\n",
			ino, name, size);
	}

	if (inode->is_symlink) {
		/* Sorry, no race free way to getxattr on symlink. */
		saverr = EPERM;
		goto out;
	}

	sprintf(procname, "/proc/self/fd/%i", inode->fd);

	if (size) {
		value = (char *)malloc(size);
		if (!value)
			goto out_err;

		ret = getxattr(procname, name, value, size);
		if (ret == -1)
			goto out_err;
		saverr = 0;
		if (ret == 0)
			goto out;

		fuse_reply_buf(req, value, ret);
	} else {
		ret = getxattr(procname, name, NULL, 0);
		if (ret == -1)
			goto out_err;

		fuse_reply_xattr(req, ret);
	}
out_free:
	free(value);
	return;

out_err:
	saverr = errno;
out:
	fuse_reply_err(req, saverr);
	goto out_free;
}

static void fs_listxattr(fuse_req_t req, fuse_ino_t ino, size_t size)
{
	char *value = NULL;
	char procname[64];
	struct fs_inode *inode = fs_inode(req, ino);
	ssize_t ret;
	int saverr;

	saverr = ENOSYS;
	if (!fs_data(req)->xattr)
		goto out;

	if (fs_debug(req)) {
		fprintf(stderr, "fs_listxattr(ino=%" PRIu64 ", size=%zd)\n",
			ino, size);
	}

	if (inode->is_symlink) {
		/* Sorry, no race free way to listxattr on symlink. */
		saverr = EPERM;
		goto out;
	}

	sprintf(procname, "/proc/self/fd/%i", inode->fd);

	if (size) {
		value = (char*)malloc(size);
		if (!value)
			goto out_err;

		ret = listxattr(procname, value, size);
		if (ret == -1)
			goto out_err;
		saverr = 0;
		if (ret == 0)
			goto out;

		fuse_reply_buf(req, value, ret);
	} else {
		ret = listxattr(procname, NULL, 0);
		if (ret == -1)
			goto out_err;

		fuse_reply_xattr(req, ret);
	}
out_free:
	free(value);
	return;

out_err:
	saverr = errno;
out:
	fuse_reply_err(req, saverr);
	goto out_free;
}

static void fs_setxattr(fuse_req_t req, fuse_ino_t ino, const char *name,
			const char *value, size_t size, int flags)
{
	char procname[64];
	struct fs_inode *inode = fs_inode(req, ino);
	ssize_t ret;
	int saverr;

	saverr = ENOSYS;
	if (!fs_data(req)->xattr)
		goto out;

	if (fs_debug(req)) {
		fprintf(stderr, "fs_setxattr(ino=%" PRIu64 ", name=%s value=%s size=%zd)\n",
			ino, name, value, size);
	}

	if (inode->is_symlink) {
		/* Sorry, no race free way to setxattr on symlink. */
		saverr = EPERM;
		goto out;
	}

	sprintf(procname, "/proc/self/fd/%i", inode->fd);

	ret = setxattr(procname, name, value, size, flags);
	saverr = ret == -1 ? errno : 0;

out:
	fuse_reply_err(req, saverr);
}

static void fs_removexattr(fuse_req_t req, fuse_ino_t ino, const char *name)
{
	char procname[64];
	struct fs_inode *inode = fs_inode(req, ino);
	ssize_t ret;
	int saverr;

	saverr = ENOSYS;
	if (!fs_data(req)->xattr)
		goto out;

	if (fs_debug(req)) {
		fprintf(stderr, "fs_removexattr(ino=%" PRIu64 ", name=%s)\n",
			ino, name);
	}

	if (inode->is_symlink) {
		/* Sorry, no race free way to setxattr on symlink. */
		saverr = EPERM;
		goto out;
	}

	sprintf(procname, "/proc/self/fd/%i", inode->fd);

	ret = removexattr(procname, name);
	saverr = ret == -1 ? errno : 0;

out:
	fuse_reply_err(req, saverr);
}


//#ifdef __cplusplus
//extern "C" {
//#endif

static struct fuse_lowlevel_ops fs_oper;

void oper_init()
{
	fs_oper.init		= fs_init;
	fs_oper.lookup		= fs_lookup;
	fs_oper.mkdir		= fs_mkdir;
	fs_oper.mknod		= fs_mknod;
	//.symlink	= fs_symlink,
	//.link		= fs_link,
	fs_oper.unlink		= fs_unlink;
	fs_oper.rmdir		= fs_rmdir;
	//.rename		= fs_rename,
	//.forget		= fs_forget,
	//.forget_multi	= fs_forget_multi,
	fs_oper.getattr	= fs_getattr;
	fs_oper.setattr	= fs_setattr,
	//.readlink	= fs_readlink,
	fs_oper.opendir	= fs_opendir;
	fs_oper.readdir	= fs_readdir;
	fs_oper.readdirplus	= fs_readdirplus;
	fs_oper.releasedir	= fs_releasedir;
	//.fsyncdir	= fs_fsyncdir,
	fs_oper.create		= fs_create,
	fs_oper.open		= fs_open,
	fs_oper.release	= fs_release,
	//.flush		= fs_flush,
	//.fsync		= fs_fsync,
	fs_oper.read		= fs_read,
	fs_oper.write      = fs_write,
	//.write_buf      = fs_write_buf,
	fs_oper.statfs		= fs_statfs;
	//.fallocate	= fs_fallocate,
	//.flock		= fs_flock,
	//.getxattr	= fs_getxattr,
	//.listxattr	= fs_listxattr,
	//.setxattr	= fs_setxattr,
	//.removexattr	= fs_removexattr,
}

int main(int argc, char *argv[])
{
	struct fuse_args args = FUSE_ARGS_INIT(argc, argv);
	struct fuse_session *se;
	struct fuse_cmdline_opts opts;
	struct fs_data fs;
     fs.debug = 0;
	 fs.writeback = 0 ;

    int ret = -1;

	/* Don't mask creation mode, kernel already did that */
	umask(0);

	pthread_mutex_init(&fs.mutex, NULL);
	fs.root.next = fs.root.prev = &fs.root;
	//lo.root.fd = -1;
	fs.cache = CACHE_NORMAL;

	if (fuse_parse_cmdline(&args, &opts) != 0)
		return 1;
	if (opts.show_help) {
		printf("usage: %s [options] <mountpoint>\n\n", argv[0]);
		fuse_cmdline_help();
		fuse_lowlevel_help();
		ret = 0;
		goto err_out1;
	} else if (opts.show_version) {
		printf("FUSE library version %s\n", fuse_pkgversion());
		fuse_lowlevel_version();
		ret = 0;
		goto err_out1;
	}

	if(opts.mountpoint == NULL) {
		printf("usage: %s [options] <mountpoint>\n", argv[0]);
		printf("       %s --help\n", argv[0]);
		ret = 1;
		goto err_out1;
	}

	if (fuse_opt_parse(&args, &fs, fs_opts, NULL)== -1)
		return 1;

	fs.debug = opts.debug;
	fs.root.refcount = 2;
    /*
	if (lo.source) {
		struct stat stat;
		int res;

		res = lstat(lo.source, &stat);
		if (res == -1)
			err(1, "failed to stat source (\"%s\")", lo.source);
		if (!S_ISDIR(stat.st_mode))
			errx(1, "source is not a directory");

	} else {
		lo.source = "/";
	}
	lo.root.is_symlink = false;
	*/
	if (!fs.timeout_set) {
		switch (fs.cache) {
		case CACHE_NEVER:
			fs.timeout = 0.0;
			break;

		case CACHE_NORMAL:
			fs.timeout = 1.0;
			break;

		case CACHE_ALWAYS:
			fs.timeout = 86400.0;
			break;
		}
	} else if (fs.timeout < 0) {
		errx(1, "timeout is negative (%lf)", fs.timeout);
	}
    /*
	lo.root.fd = open(lo.source, O_PATH);
	if (lo.root.fd == -1)
		err(1, "open(\"%s\", O_PATH)", lo.source);
    */

    {
        //printf("fsname: %s \n", lo.source);
        char fsname[] = "raptorfs1";
    
	    ret = mds_mountfs(&fs.root, fsname);   
    }

    oper_init();

	se = fuse_session_new(&args, &fs_oper, sizeof(fs_oper), &fs);
	if (se == NULL)
	    goto err_out1;

	if (fuse_set_signal_handlers(se) != 0)
	    goto err_out2;

	if (fuse_session_mount(se, opts.mountpoint) != 0)
	    goto err_out3;

	fuse_daemonize(opts.foreground);

	/* Block until ctrl+c or fusermount -u */
	if (opts.singlethread)
		ret = fuse_session_loop(se);
	else
		ret = fuse_session_loop_mt(se, opts.clone_fd);

	fuse_session_unmount(se);
err_out3:
	fuse_remove_signal_handlers(se);
err_out2:
	fuse_session_destroy(se);
err_out1:
	free(opts.mountpoint);
	fuse_opt_free_args(&args);

	if (fs.root.fd >= 0)
		close(fs.root.fd);

    mds_umountfs(&fs);

	return ret ? 1 : 0;
}

//#ifdef __cplusplus
//}
//#endif

