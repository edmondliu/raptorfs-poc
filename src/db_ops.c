
#include "rocksdb/c.h"

#include "db_ops.h"


const char DBPath[] = "/mnt/stor_kv";
const char SimLastVidKeyName[] = "simulated_last_vid_key";


//const char DBBackupPath[] = "/mnt/kvfs_backup";

//to pass db, iter as handle for rpc usage later.
//TODO: use db, iter pointer directly in param since rpc is not needed anymore. one db for each fs. 
rocksdb_t *db;
//rocksdb_backup_engine_t *be;

#define DEFAULT_VID (0x1248201901010000)
uint64_t g_sim_vid = DEFAULT_VID;    // 32bit for blocknum

uint64_t rpc_master_get_vid()
{
	char *err = NULL;
        char vid_str_put[MAX_VALUE_SIZE];
        rocksdb_writeoptions_t *writeoptions = rocksdb_writeoptions_create();

        g_sim_vid++;
        printf("get new fake vid(0x%llx)\n", g_sim_vid);
        sprintf(vid_str_put,"%llx", g_sim_vid);
        rocksdb_put(db, writeoptions, SimLastVidKeyName, strlen(SimLastVidKeyName), vid_str_put, strlen(vid_str_put) + 1, &err);
        assert(!err);

        free(writeoptions);

	return (g_sim_vid);
}

int db_open()
{
    rocksdb_options_t *options = rocksdb_options_create();
    rocksdb_options_set_create_if_missing(options, 1);
    rocksdb_options_set_prefix_extractor(options, rocksdb_slicetransform_create_fixed_prefix(34));
    
    // open DB
    char *err = NULL;
    db = rocksdb_open(options, DBPath, &err);
    assert((!err) && (db));
    
    // open Backup Engine that we will use for backing up our database
    //be = rocksdb_backup_engine_open(options, DBBackupPath, &err);
    //assert(!err);

    //make fake vid persistent in db so that fs could be re-mounted.
    size_t len;   
    rocksdb_readoptions_t *readoptions = rocksdb_readoptions_create();

    char *vid_str = rocksdb_get(db, readoptions, SimLastVidKeyName, strlen(SimLastVidKeyName), &len, &err);
    assert(!err);
    if(vid_str != NULL){    
        g_sim_vid = strtoull(vid_str, NULL, 16) + 1;
        printf("Get existing fake vid(0x%llx)\n", g_sim_vid);
        
        free(vid_str);
        free(readoptions);
    }
    else{
        char vid_str_put[MAX_VALUE_SIZE];
        rocksdb_writeoptions_t *writeoptions = rocksdb_writeoptions_create();  

        g_sim_vid = DEFAULT_VID;
        printf("Initialize fake vid(0x%llx)\n", g_sim_vid);        
        sprintf(vid_str_put,"%llx", g_sim_vid);
        rocksdb_put(db, writeoptions, SimLastVidKeyName, strlen(SimLastVidKeyName), vid_str_put, strlen(vid_str_put) + 1, &err);
        assert(!err);     
        
        free(writeoptions);
    }    

    return 0;
}

int db_close()
{
    rocksdb_close(db);

    return 0;
}

int db_get_fs(const char *fsname, uint64_t *vid)
{
    assert(fsname);
    char *err = NULL;
    size_t len;   
    rocksdb_readoptions_t *readoptions = rocksdb_readoptions_create();

    char *vid_str = rocksdb_get(db, readoptions, fsname, strlen(fsname), &len, &err);
    assert(!err);
    if(vid_str == NULL){
	    *vid = 0;
        return -1;        
	}

    *vid = strtoull(vid_str, NULL, 16);
    free(vid_str);
    free(readoptions);
    
    return 0;
}

int db_put_fs(const char *fsname, uint64_t *vid)
{
    char vid_str[MAX_VALUE_SIZE];
    char *err = NULL;
    *vid = rpc_master_get_vid();
	printf("put fsid(0x%llx)\n", *vid);
    rocksdb_writeoptions_t *writeoptions = rocksdb_writeoptions_create();  
    
    sprintf(vid_str,"%llx", *vid);
    rocksdb_put(db, writeoptions, fsname, strlen(fsname), vid_str, strlen(vid_str) + 1, &err);
    assert(!err); 

    free(writeoptions);

    return 0;
}

int db_open_ino(uint64_t fs_id, uint64_t inode_id, const char* name, uint64_t *vid)
{
    size_t len;
    char *err = NULL;
    char key_str[MAX_KEY_SIZE];   
    rocksdb_readoptions_t *readoptions = rocksdb_readoptions_create();

    sprintf(key_str,"%llx/%llx/%s", fs_id, inode_id, name);
    char *vid_str = rocksdb_get(db, readoptions, key_str, strlen(key_str), &len, &err);
    assert(!err);

    if(vid_str == NULL){
	    *vid = 0;
        return -1;        
	}

    *vid = strtoull(vid_str, NULL, 16);
    free(vid_str);
    free(readoptions); 
    
    return 0;
}

int db_get_ino(uint64_t fs_id, uint64_t inode_id, const char *name, uint64_t *vid)
{
    size_t len;
    char *err = NULL;
    char key_str[MAX_KEY_SIZE];   
    rocksdb_readoptions_t *readoptions = rocksdb_readoptions_create();

    sprintf(key_str,"%llx/%llx/%s", fs_id, inode_id, name);
    char *vid_str = rocksdb_get(db, readoptions, key_str, strlen(key_str), &len, &err);
    assert(!err);

    if(vid_str == NULL){
	    *vid = 0;
        return -1;        
	}

    *vid = strtoull(vid_str, NULL, 16);
    free(vid_str);
    free(readoptions); 
    
    return 0;
}

int db_put_ino(uint64_t fs_id, uint64_t inode_id, const char *name, uint64_t *vid)
{
    char *err = NULL;
    char key_str[MAX_KEY_SIZE];   
    char vid_str[MAX_VALUE_SIZE];
    rocksdb_writeoptions_t *writeoptions = rocksdb_writeoptions_create();      

    sprintf(key_str,"%llx/%llx/%s", fs_id, inode_id, name);
    *vid = rpc_master_get_vid();  
	printf("put inode(0x%llx)\n", *vid);  
    sprintf(vid_str,"%llx", *vid);
    rocksdb_put(db, writeoptions, key_str, strlen(key_str), vid_str, strlen(vid_str) + 1, &err);
    assert(!err);

    free(writeoptions);
    return 0;
}

int db_delete_ino(uint64_t fs_id, uint64_t inode_id, const char *name)
{
    char *err = NULL;
    char key_str[MAX_KEY_SIZE];   
    rocksdb_writeoptions_t *writeoptions = rocksdb_writeoptions_create();      

    sprintf(key_str,"%llx/%llx/%s", fs_id, inode_id, name);
    rocksdb_delete(db, writeoptions, key_str, strlen(key_str), &err);
    assert(!err);

    free(writeoptions);
    return 0;
}

void* db_iter_create(uint64_t fs_id, uint64_t inode_id, off_t offset)
{
    char prefix_str[MAX_KEY_SIZE];
    rocksdb_readoptions_t *readoptions = rocksdb_readoptions_create();
    rocksdb_iterator_t *iter = rocksdb_create_iterator( db, readoptions );
    
    rocksdb_readoptions_set_prefix_same_as_start(readoptions, true);
    sprintf(prefix_str,"%llx/%llx/", fs_id, inode_id);
    
    rocksdb_iter_seek(iter, prefix_str, strlen(prefix_str));

    printf("Iter create for prefix(%s), offset(%d)\n", prefix_str, offset);
    //a temp usage of offset to be enhanced
    for(int i=0; i<offset; i++)
    {       
        rocksdb_iter_next(iter);
    }
        
    return iter;
}

int db_iter_destroy(void *iter_ptr)
{
    rocksdb_iterator_t *iter = (rocksdb_iterator_t *)iter_ptr;

    rocksdb_iter_destroy(iter);

    return 0;
}

int db_iter_seek(void *iter_ptr, off_t offset_cur, off_t offset_to)
{
    int i, delta = 0;
    rocksdb_iterator_t *iter = (rocksdb_iterator_t *)iter_ptr;
    
    printf("db_iter_seek: offset_cur(%llu), offset_to(%llu)\n", offset_cur, offset_to);
    if(offset_cur < offset_to) {
        delta = offset_to - offset_cur;
        for( i=0; i<delta; i++)
        {       
            rocksdb_iter_next(iter);
        }
    }
    else if( offset_cur > offset_to ){
        delta = offset_cur - offset_to;
        for( i=0; i<delta; i++)
        {       
            rocksdb_iter_prev(iter);
        }
    }

    return 0;
}

int db_iter_next(void *iter_ptr, char *name, uint64_t *vid)
{
    const char* key_str;
    const char* vid_str;
    size_t klen;
    size_t vlen;
    rocksdb_iterator_t *iter = (rocksdb_iterator_t *)iter_ptr;

    if(!rocksdb_iter_valid(iter))
        return -1;

    key_str = rocksdb_iter_key(iter, &klen);
    //name = key_str;
    //sprintf(name, key_str);
    memcpy(name, key_str, klen);
    
    vid_str = rocksdb_iter_value(iter, &vlen);
    *vid = strtoull(vid_str, NULL, 16);

    printf("Iter next:  key(%s), value(%s)\n", key_str, vid_str);

    rocksdb_iter_next(iter);

    return 0;
}

int db_iter_prev(void *iter_ptr, char *name, uint64_t *vid)
{
    const char* key_str;
    const char* vid_str;
    size_t klen;
    size_t vlen;
    rocksdb_iterator_t *iter = (rocksdb_iterator_t *)iter_ptr;

    if(!rocksdb_iter_valid(iter))
        return -1;

    rocksdb_iter_prev(iter);
 /* not needed at now
    key_str = rocksdb_iter_key(iter, &klen);
    memcpy(name, key_str, klen);
    
    vid_str = rocksdb_iter_value(iter, &vlen);
    *vid = strtoull(vid_str, NULL, 16);

    printf("Iter prev:  key(%s), value(%s)\n", key_str, vid_str);  
*/
    return 0;
}

